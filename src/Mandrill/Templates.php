<?php

class Mandrill_Templates {
    public function __construct(Mandrill $master) {
        $this->master = $master;
    }

    /**
     * Add a new template
     * @param string $name
     * @param string $from_email
     * @param string $subject
     * @param string $code
     * @param string $text
     * @param boolean $publish
     * @param array $labels
     *
     * @return struct the infomation saved about the new template
     *  - slug
     *  - name
     *  - labels 
     *  - code
     *  - subject
     *  - from_email
     *  - from_name
     *  - text
     *  - publish_name
     *  - publish_code
     *  - publish_subject
     *  - publish_text
     *  - published_at
     *  - create_at
     *  - updated_at
     */
    function add($name, $from_email=null, $from_name=null, $subject=null, $code=null, $text=null, $publish=true, $labels=array()) {
        $_params = array(
            "name" => $name,
            "from_email" => $from_email,
            "from_name" => $from_name,
            "subject" => $subject,
            "code" => $code,
            "text" => $text,
            "publish" => $publish,
            "labels" => $labels
        );
        return $this->master->call('template/add', $_params);
    }

    /**
     * get the infomation for an existing template
     * @param string $name
     */
    function info($name) {
        $_params = array("name" => $name);
        return $this->master->call('template/info', $_params);
    }

    /**
     * Update an existing template
     * @param string $name
     * @param string $from_email
     * @param string $subject
     * @param string $code
     * @param string $text
     * @param boolean $publish
     * @param array $labels
     */
    function update($name, $from_email, $from_name=null, $subject=null, $code=null, $text=null, $publish=true, $labels=null) {
        $_params = array(
            "name" => $name,
            "from_email" => $from_email,
            "from_name" => $from_name,
            "subject" => $subject,
            "code" => $code,
            "text" => $text,
            "publish" => $publish,
            "labels" => $labels
        );
        return $this->master->call('template/update', $_params);
    }

    /**
     * Publish the content for the template
     * @param string $name
     * @return struct
     *  - slug
     *  - name
     *  - labels 
     *  - code
     *  - subject
     *  - from_email
     *  - from_name
     *  - text
     *  - publish_name
     *  - publish_code
     *  - publish_subject
     *  - publish_text
     *  - published_at
     *  - create_at
     *  - updated_at
     */
    function publish($name) {
        $_params = array("name" => $name);
        return $this->master->call('template/publish', $_params);
    }

    /**
     * Delete a template
     * @param string $name
     * @return struct
     *  - slug
     *  - name
     *  - labels
     *  - code
     */
    function delete($name) {
        $_params = array(
            "name" => $name
        );
        return $this->master->call('template/delete', $_params);
    }

    /**
     * Return a list of all the templates
     * @param string $label
     */
    function list($label=null) {
        $_params = array("label" => $label);
        return $this->master->call('template/list', $_params);
    }

    /**
     * Return the recent history
     * @param string $name
     * @return array the array of history infomation
     *  - return[] struct
     *      - time string
     *      - sent integer
     *      - hard_bounces integer
     *      - soft_bounces integer
     *      - rejects integer
     *      - complaints integer
     *      - opens integer
     *      - unique_open integer
     */
    function timeSeries($name) {
        $_params = array("name" => $name);
        return $this->master->call('template/time-series', $_params);
    }

    /**
     * Inject content and merge fields into a template
     * @param string $template_name
     * @param array $template_content
     *  - template_content[] struct the injection
     *      - name string 
     *      - content string 
     * @param array $merge_vars
     * @return struct the result of rendering the given template
     *  - html string
     */
    function render($template_name, $template_content, $merge_vars=null) {
        $_params = array(
            "template_name" => $template_name
        );
    }
}
?>
