<?php
class Mandrill_Messages {
    protected function __construct(Mandrill $master) {
        $this->master = $master;
    }

    /**
     * Send a new message
     * @param struct $message
     *  - html string
     *  - text 
     * @param boolean $async
     */
    public function send($message, $async=false, $ip_pool=null, $send_at=null) {
        $_params = array(
            "message" => $message,
            "async" => $async,
            "ip_pool" => $ip_pool,
            "send_at" => $send_at
        );
        return $this->master->call('message/send', $_params);
    }

    /**
     * @param string $template_name
     * @param array $template_content
     * @param struct $message
     *  - html string
     *  - text 
     * @param boolean $async
     * @param string $ip_pool
     * @param string $send_at
     */
    public function sendTemplate($template_name, $template_content, $message, $async=false, $ip_pool=null, $send_at=null) {
        $_params = array(
            "template_name" => $template_name,
            "template_content" => $template_content,
            "message" => $message,
            "async" => $async,
            "ip_pool" => $ip_pool,
            "send_at" => $send_at
        );
        return $this->master->call('message/send-template', $_params);
    }
}
?>
