<?php

class Mandrill_Exports {
    public function __construct($master) {
        $this->master = $master;
    }

    public function info($id) {
        $_params = array("id" => $id);
        return $this->master->call('exports/info', $_params);
    }

    /**
     * Return a list of exports
     * @return array exports
     *  - return[] struct
     *      - id string
     *      - created_at string
     *      - type string
     *      - finished_at string
     *      - state string
     *      - result_url string
     */
    function list() {
        $_params = array();
        return $this->master->call('exports/list', $_params);
    }

    function rejects($notify_email=null) {
        $_params = array("notify_email" => $notify_email);
        return $this->master->call('exports/rejects', $_params);
    }

    function whitelist($notify_email) {
        $_params = array("notify_email" => $notify_email);
        return $this->master->call('exports/whitelist', $_params);
    }

    /**
     * activity history
     * @param string $notify_email
     * @param string $date_from
     * @param string $date_to
     * @param array $tags
     *  - tags[] string
     * @param array $states
     *  - states[] string 
     * @param array $api_keys
     *  - api_keys[] string an API key
     */
    function activity($notify_email=null, $date_from=null, $date_to=null, $tags=null, $senders=null, $states=null, $api_keys=null) {
        $_params = array(
            "notify_email" => $notify_email,
            "date_from" => $date_from,
            "date_to" => $date_to,
            "tags" => $tags,
            "senders" => $senders,
            "states" => $states,
            "api_keys" => $api_keys
        );
    }
}
?>
