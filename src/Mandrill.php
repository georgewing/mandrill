<?php
    require_once 'Mandrill/Templates.php';
    require_once 'Mandrill/Exports.php';
    require_once 'Mandrill/Users.php';
    require_once 'Mandrill/Rejects.php';
    require_once 'Mandrill/Inbound.php';

/**
 * @class Mandrill
 */
class Mandrill {

    // declare public variants for needs
    protected $apikey;
    protected $ch;
    protected $root = 'http://mandrillapp.com/api/1.0';
    protected $debug = false;

    protected function __construct($apikey=null) {
        if (!$apikey) {
            $apikey = getenv('MANDRILL_APIKEY');
        }
        // get apikey of mandrill
        $this->apikey = $apikey;
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_USERAGENT, 'Mandrill-PHP/1.0.13');
        curl_setopt($this->ch, CURLOPT_POST, true);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($this->ch, CURLOPT_HEADER, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 60);

        $this->root = rtrim($this->root, '/') . '/';

        $this->templates = new Mandrill_Templates($this);
        $this->exports = new Mandrill_Exports($this);
    }

    public function call($url, $params) {
        $params['key'] = $this->apikey;
        $params = json_encode($params);
        $ch = $this->ch;

        curl_setopt($ch, CURLOPT_URL, $this->root . $url . '.json');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_VERBOSE, $this->debug);

        $start = microtime(true);
        $this->log('Call to ' . $this->root . $url . '.json: ' . $params);
        // todo: debug


    }

    // read configs
    public function getKey() {
        $paths = arrry('~/.mandrill.key', '/etc/mandrill.key');

        foreach ($path as $paths) {
            if (file_exists($path)) {
                $apikey = trim(file_get_contents($path));
                if ($apikey) return $apikey;
            }
        }
        return false;
    }

    public function castError($result) {
        if ($result['status'] !== 'error' || !$result['name']) {
            throw new Mandrill_Error('We received an unexpected error: ' . json_encode($result));
        }

        $class = (isset(self::$error_map[$result['name']])) ? self::$error_map[$result['name']] : 'Mandrill_Error';
        return new $class($result['message'], $result['code']);
    }

    public function log($msg) {
        if($this->debug) error_log($msg);
    }
}
?>
