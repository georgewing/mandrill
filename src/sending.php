<?php
require 'Mandrill.php';

$mandrill = new Mandrill();

$message = array(
    "subject" => "Test Message",
    "from_email" => "",
    "html" => "<p>This is a test message with Mandrill\'s PHP wrapper.</p>'",
    "to" => array(array("email" => "", "name" => "")),
    "merge_vars" => array(array(
        "rcpt" => "",
        "vars" =>
        array(
            array(
                "name" => "FIRSTNAME",
                "content" => "Recipient 1 first name"
            ),
            array(
                "name" => "LASTNAME",
                "content" => "Last Name"
            )
        )));
);

$template_name = 'Stationary';

$template_content = array(
    array(
        "name" => "main",
        "content" => "Hi *|FIRSTNAME|* *|LASTNAME|*, thanks for signing up."
    ),
    array(
        "name" => "footer",
        "content" => "Copyright 2015"
    )
);

print_r($mandrill->message->sendTemplate($template_name, $template_content, $message));
?>
